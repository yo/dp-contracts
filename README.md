# Devparty Contracts

## Deploy

### Rinkeby

```sh
npx hardhat run deploy.js --network rinkeby
```

### Mumbai

```sh
npx hardhat run deploy.js --network mumbai
```

### Mainet

```sh
npx hardhat run deploy.js --network mainet
```

### Matic

```sh
npx hardhat run deploy.js --network matic
```
